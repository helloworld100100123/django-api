# Generated by Django 3.2.4 on 2021-06-07 21:45

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RecommendationLog',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sent', models.DateTimeField(default=django.utils.timezone.now)),
                ('user_id', models.TextField(blank=True, null=True)),
                ('video_id', models.TextField(blank=True, null=True)),
            ],
        ),
    ]

from django.db import models
from django.utils import timezone

# Create your models here.


class RecommendationLog(models.Model):
    sent = models.DateTimeField(default=timezone.now)
    user_id = models.TextField(blank=True, null=True)
    video_id = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.user_id


from django.test import TestCase
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APITestCase

from .models import RecommendationLog

# Create your tests here.


class RecommendationLogModelTests(TestCase):
    """ tests for RecommendationLog Model """

    def test_recommendation_is_created(self):
        """test RecommendationLog is created"""

        self.assertEqual(RecommendationLog.objects.last(), None)

        timezone_now = timezone.now()
        new_log = RecommendationLog.objects.create(sent=timezone_now,
                                                   user_id='testuser123',
                                                   video_id='wbn007')
        new_log.save()

        self.assertEqual(RecommendationLog.objects.last().sent, timezone_now)
        self.assertEqual(RecommendationLog.objects.last().user_id, 'testuser123')
        self.assertEqual(RecommendationLog.objects.last().video_id, 'wbn007')


class RecommendationTestCase(APITestCase):
    """ test /api/recommendation/ """

    def test_get_recommendation(self):
        """ test get request """

        responce = self.client.get('/api/recommendation/')

        self.assertEqual(responce.data['Hello'], "world!")

    def test_post_recommendation(self):
        """ test post request """

        data = {"user_id": "testuser123"}
        responce = self.client.post('/api/recommendation/', data)

        self.assertEqual(responce.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(responce.data['video_ids']), 3)

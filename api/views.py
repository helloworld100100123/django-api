
import pandas as pd

from pathlib import Path
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django_api.settings import DATA_DIR
from . models import RecommendationLog


def get_recommendation(user_id=None):
    '''get random video_id from csv'''

    filepath = Path(DATA_DIR, 'Chatbot Next Step Content-ALL CONTENT.csv')

    df = pd.read_csv(filepath, usecols=['Video Short Code (6 digit)'])
    df = df.dropna()

    video_ids = df.sample(3)['Video Short Code (6 digit)']
    return video_ids


class RecommendationApi(APIView):
    """
    """

    def get(self, request, format=None):
        data = {'Hello': 'world!'}
        return Response(data)

    def post(self, request, format=None):

        data = {}

        if not request.body:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        body = request.data

        user_id = body['user_id']
        video_ids = get_recommendation(user_id)

        objs = [RecommendationLog(user_id=user_id, video_id=vid) for vid in video_ids]
        RecommendationLog.objects.bulk_create(objs)

        data['user_id'] = user_id
        data['video_ids'] = video_ids
        if data:
            return Response(data, status=201)
        return Response(status=status.HTTP_400_BAD_REQUEST)
